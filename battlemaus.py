#! /usr/bin/env python3

# Copyright (C) 2022 Ross Burton <ross@burtonini.com>
# MIT licensed

import collections
import dataclasses
import itertools
import random
import typing
import unittest
import unittest.mock
import enum


class State(enum.Enum):
    ALIVE = enum.auto()
    INCAPACITATED = enum.auto()
    DEAD = enum.auto()


@dataclasses.dataclass
class Player:
    """Player"""

    name: str
    str: int
    dex: int
    wil: int
    hp: int
    # int is lame, parse dice expression
    attack: int
    armour: int
    state: State = State.ALIVE
    # grit?

    def copy(self) -> "Player":
        return dataclasses.replace(self)


def roll(sides: int) -> int:
    return random.randint(1, sides)


def save(target: int) -> bool:
    return roll(20) <= target


def do_round(attacker: Player, defender: Player) -> bool:
    """
    Perform a single round of combat between the attacker and defender. Returns
    True if the attacker won (the defender is dead or incapacitated), False if
    the fight should continue.
    """

    # TODO impaired/advantage?
    # print(f"{attacker=} {defender=}")

    # Roll your weapon’s die and do that much damage to an opponent, ...
    damage = roll(attacker.attack)
    # print(f"{attacker.name} attacks with damage {damage}")

    # ... minus their armour.
    damage -= defender.armour
    if damage <= 0:
        return False

    # Damage is dealt first to a creature’s Hit Protection (HP).
    defender.hp -= damage
    if defender.hp < 0:
        damage = -defender.hp
        defender.hp = 0
    # print(f"{defender=}, {damage=}")

    # Once HP is depleted, damage is dealt to STR.
    if defender.hp > 0:
        return False

    defender.str -= damage
    defender.str = max(defender.str, 0)
    if defender.str <= 0:
        defender.state = State.DEAD
        return True

    # After taking STR damage, the creature must make a STR save. If
    # they succeed, they are still able to fight. If they fail, they
    # take critical damage.
    if not save(defender.str):
        defender.state = State.INCAPACITATED
        return True

    return False


def do_combat(pc_a: Player, pc_b: Player) -> typing.Tuple[Player, Player]:
    """
    Perform as many rounds of combat (do_round) as needed between two players.
    The pc_a has the initiative and takes the first strike.
    """
    # TODO This will loop forever if neither attacker can make damage. Detect
    # and abandon, or have a limit of 100 iterations.
    rounds = itertools.cycle(((pc_a, pc_b), (pc_b, pc_a)))
    for attacker, defender in rounds:
        won = do_round(attacker, defender)
        if won:
            return attacker, defender
    # TODO not sure about this
    raise Exception("Stalemate")


def pick_initiative(pc_a: Player, pc_b: Player) -> typing.Tuple[Player, Player]:
    if save(pc_a.dex):
        return (pc_a, pc_b)
    else:
        return (pc_b, pc_a)


def make_mouse() -> Player:
    return Player(name="Mouse", str=9, dex=9, wil=9, hp=3, attack=6, armour=1)


def make_rat() -> Player:
    return Player(name="Rat", str=12, dex=8, wil=8, hp=3, attack=6, armour=0)


def make_ants() -> Player:
    return Player(name="Ant", hp=1, str=6, dex=8, wil=8, armour=1, attack=7)


def make_worm() -> Player:
    return Player(name="Slow worm", hp=4, str=10, dex=8, wil=6, armour=0, attack=4)


def make_owl() -> Player:
    return Player(name="Owl", hp=15, str=15, dex=15, wil=15, armour=1, attack=10)


def make_stag() -> Player:
    return Player(name="Stag Beetle", hp=4, str=10, dex=5, wil=8, armour=1, attack=8)


def make_minotaur() -> Player:
    return Player(name="Minotaur Beetle", hp=3, str=9, dex=7, wil=7, armour=1, attack=4)


if __name__ == "__main__":
    scores = collections.Counter()  # type: typing.Counter[typing.Optional[str]]
    for _ in range(0, 100):
        players = pick_initiative(make_mouse(), make_rat())
        print(f"{players[0].name} goes first.")

        winner, looser = do_combat(*players)
        print(f"{winner.name} won, {looser.name} is {looser.state.name.lower()}.")
        scores[winner.name] += 1
    print(scores)


class Tests(unittest.TestCase):
    def test_saves(self):
        with unittest.mock.patch("battlemaus.roll", side_effect=[1, 10, 20]):
            self.assertTrue(save(10))
            self.assertTrue(save(10))
            self.assertFalse(save(10))

    def test_initiative(self):
        with unittest.mock.patch("battlemaus.roll", side_effect=[1, 9, 15]):
            mouse = make_mouse()
            rat = make_rat()

            first, second = pick_initiative(mouse, rat)
            self.assertIs(first, mouse)
            self.assertIs(second, rat)
            first, second = pick_initiative(mouse, rat)
            self.assertIs(first, mouse)
            self.assertIs(second, rat)
            first, second = pick_initiative(mouse, rat)
            self.assertIs(first, rat)
            self.assertIs(second, mouse)

    def test_absorbed_by_armour(self):
        with unittest.mock.patch(
            "battlemaus.roll",
            side_effect=[
                1,
            ],
        ):
            mouse = make_mouse()
            owl = make_owl()

            won = do_round(mouse, owl)
            self.assertFalse(won)
            self.assertEqual(owl.hp, 15)
            self.assertEqual(owl.str, 15)
            self.assertEqual(owl.state, State.ALIVE)

    def test_mouse_vs_owl(self):
        with unittest.mock.patch(
            "battlemaus.roll", side_effect=[8, 8, 8, 1, 8, 1, 8, 1]
        ):
            mouse = make_mouse()
            owl = make_owl()

            # 8 damage from the owl, mouse passes every str save
            won = do_round(mouse, owl)
            self.assertFalse(won)
            self.assertEqual(owl.hp, 8)
            self.assertEqual(owl.str, 15)

            won = do_round(mouse, owl)
            self.assertFalse(won)
            self.assertEqual(owl.hp, 1)
            self.assertEqual(owl.str, 15)

            won = do_round(mouse, owl)
            self.assertFalse(won)
            self.assertEqual(owl.hp, 0)
            self.assertEqual(owl.str, 9)

            won = do_round(mouse, owl)
            self.assertFalse(won)
            self.assertEqual(owl.hp, 0)
            self.assertEqual(owl.str, 2)
            self.assertEqual(owl.state, State.ALIVE)

            won = do_round(mouse, owl)
            self.assertTrue(won)
            self.assertEqual(owl.hp, 0)
            self.assertEqual(owl.str, 0)
            self.assertEqual(owl.state, State.DEAD)

    def test_owl_vs_mouse_dead(self):
        rolls = [10, 1, 10]
        with unittest.mock.patch("battlemaus.roll", side_effect=rolls):
            mouse = make_mouse()
            owl = make_owl()
            # Damage 10, str save passed
            won = do_round(owl, mouse)
            self.assertFalse(won)
            self.assertEqual(mouse.hp, 0)
            self.assertEqual(mouse.str, 3)
            self.assertEqual(mouse.state, State.ALIVE)

            # Damage 10
            won = do_round(owl, mouse)
            self.assertTrue(won)
            self.assertEqual(mouse.hp, 0)
            self.assertEqual(mouse.str, 0)
            self.assertEqual(mouse.state, State.DEAD)

    def test_owl_vs_mouse_incapacitated(self):
        rolls = [10, 10]
        with unittest.mock.patch("battlemaus.roll", side_effect=rolls):
            mouse = make_mouse()
            owl = make_owl()
            # Damage 10, str save passed
            won = do_round(owl, mouse)
            self.assertTrue(won)
            self.assertEqual(mouse.hp, 0)
            self.assertEqual(mouse.str, 3)
            self.assertEqual(mouse.state, State.INCAPACITATED)

    def test_combat(self):
        # Both sides attack with 5 damage and pass their str saves until the
        # final blow from the mouse.
        with unittest.mock.patch(
            "battlemaus.roll", side_effect=[5, 1, 5, 1, 5, 1, 5, 1, 4, 20]
        ):
            mouse = make_mouse()
            rat = make_rat()
            winner, looser = do_combat(mouse, rat)
            self.assertIs(winner, mouse)
            self.assertIs(looser, rat)
            self.assertEqual(winner.str, 4)
            self.assertEqual(looser.str, 1)
